# BaiDuFaceOfflineSample

#### 介绍
百度人脸识别离线识别SDK Java版 Windows 基础项目搭建运行


#### 使用说明

项目自身占用挺大。DLL文件较多。下载可能会慢。耐心等待

##### 其他必要或非必要文件

face-resource 人脸识别的一些模型文件 必须要

orbe_install  奥比摄像头所需exe

hjimi_install 华捷艾米摄像头所需exe

下载后粘贴到项目根目录即可

下载地址:

链接: https://pan.baidu.com/s/1LL3Mg6LIGNq12l9k3NDK6Q 提取码: ydtb 

##### 项目整包下载



#### 参考配套博文

https://blog.csdn.net/u010651369/article/details/100543224


#### 示例代码

```
import com.jni.face.*;

public class FaceTest {

    public static void main(String[] args) {
        Face face = new Face();
        // 初始化sdk
        // 若采用证件照模式，请把id_card设为true，否则为false，证件照模式和非证件照模式提取的人脸特征值不同，
        // 不能混用
        boolean idCard = false;
        face.sdkInit(idCard);
        String deviceId = face.getDeviceId();
        System.out.println("设备指纹信息:" + deviceId);
        //图片路径
        String filePath = "F:\\testimg\\demo-card-1.jpg";
        //获取人脸属性
        String result = Face.faceAttr(filePath);
        System.out.println("result = " + result);
    }

}
```




