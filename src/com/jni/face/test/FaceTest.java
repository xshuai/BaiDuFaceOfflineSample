package com.jni.face.test;

import com.jni.face.*;

/**
 * @Description 获取人脸属性
 * @author 小帅丶
 * @className FaceTest
 * @Date 2019/9/4-16:38
 **/
public class FaceTest {

    public static void main(String[] args) {
        Face face = new Face();
        // 初始化sdk
        // 若采用证件照模式，请把id_card设为true，否则为false，证件照模式和非证件照模式提取的人脸特征值不同，
        // 不能混用
        boolean idCard = false;
        face.sdkInit(idCard);
        String deviceId = face.getDeviceId();
        System.out.println("设备指纹信息:" + deviceId);
        //图片路径
        String filePath = "F:\\testimg\\demo-card-1.jpg";
        //获取人脸属性
        String result = Face.faceAttr(filePath);
        System.out.println("result = " + result);
    }

}
